<?php

class Homepage extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        // $this->load->database();
        $this->load->model('M_homepage');
    }

    public function index()
    {
        
        $this->load->view('homepage');
    }

    public function list()
    {
        $data['barang'] = $this->M_homepage->get();
        $this->load->view('tampil',$data);
    }

    public function input()
    {
        if ($this->input->method() === 'post') {
            $barang = [
                'nama_barang' => $this->input->post('nama_barang'),
                'harga_barang' => $this->input->post('harga_barang'),
                'jumlah_barang' => $this->input->post('jumlah_barang')
            ];

            $saved = $this->M_homepage->insert($barang);

            if ($saved) {
                return redirect(base_url('index.php/homepage/list'));
            }
        }
    }

    public function delete($id = null)
    {
        $deleted = $this->M_homepage->delete($id);
        if ($deleted) {
            return redirect(base_url('index.php/homepage/list'));
        }
    }

    public function edit()
    {
        if ($this->input->method() === 'post') {
			$barang = [
				'id' => $this->input->post('id'),
				'nama_barang' => $this->input->post('nama_barang'),
				'harga_barang' => $this->input->post('harga_barang'),
				'jumlah_barang' => $this->input->post('jumlah_barang')
			];
			$updated = $this->M_homepage->edit($barang);
			if ($updated) {
				return redirect(base_url('index.php/homepage/list'));
			}
		}
    }

    public function tampilById($id = null)
    {
        $barangs = $this->M_homepage->find($id);
        foreach ($barangs as $barang ) {
            $data['barang'] = array('id'=>$barang['id'], 'nama_barang'=>$barang['nama_barang'], 'harga_barang'=>$barang['harga_barang'], 'jumlah_barang'=>$barang['jumlah_barang']);
        }
        $this->load->view('edit',$data);
    }
}