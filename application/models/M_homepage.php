<?php

class M_homepage extends CI_Model
{
    private $_table = 'barang';

    public function get()
    {
        $query = $this->db->get($this->_table);
        return $query->result();
    }

    public function insert($barang)
    {
        return $this->db->insert($this->_table, $barang);
    }

    public function delete($id)
    {
        return $this->db->delete($this->_table, ['id' => $id]);
    }

    public function edit( $barang)
    {
        $this->db->where('id', $barang['id']);
        return $this->db->update($this->_table, $barang);
    }

    public function find($id)
	{
		$this->db->where('id', $id);
        $get =  $this->db->get('barang');
        if ($get->num_rows() != 0) {
            return $get->result_array();
        } else {
            return null;
        }
    }
}