<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4-4.6.0/dt-1.11.5/b-2.2.2/sc-2.0.5/sb-1.3.2/sp-2.0.0/datatables.min.css"/>
 

    <title>Data Barang</title>
  </head>
  <body>
  <div class="container">
  <div class="table-responsive">
      <table id="tabel-barang" class="table table-striped table-hover" style="width:100%">
        <thead>
          <tr>
            <th>Id</th>
            <th>Nama Barang</th>
            <th>Harga Barang</th>
            <th>Jumlah Barang</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($barang as $barangs): ?>
          <tr>
            <td>
              <div><?= $barangs->id ?></div>
            </td>
            <td>
              <div><?= $barangs->nama_barang ?></div>
            </td>
            <td>
              <?= $barangs->harga_barang ?>
            </td>
            <td>
              <?= $barangs->jumlah_barang ?>
            </td>
            <td>
              <a href="<?= base_url('index.php/homepage/tampilById/'.$barangs->id) ?>">edit</a>
              <a href="<?= base_url('index.php/homepage/delete/'.$barangs->id) ?>">hapus</a>
            </td>
          </tr>
          <?php endforeach ?>
        </tbody>
      </table>
  </div>

      <button>
        <a href="<?= base_url()?>">kembali</a>
      </button>
  </div>

  

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4-4.6.0/dt-1.11.5/b-2.2.2/sc-2.0.5/sb-1.3.2/sp-2.0.0/datatables.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/js/bootstrap.min.js"></script>
 
    <script>
    $(document).ready( function () {
        $('#tabel-barang').dataTable();
    });
    </script>
  </body>
</html>