<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>
    <div class="container">
      <form action="<?= base_url("index.php/homepage/input")?>" method="post">
          <div class="row">
            <label class="col-lg-2" for="barang_barang">Nama Barang</label>
            <input required class="col" type="text" name="nama_barang">
          </div>
          <div class="row">
            <label class="col-lg-2" for="harga_barang">Harga</label>
            <input required class="col" type="text" name="harga_barang">
          </div>
          <div class="row">
            <label class="col-lg-2" for="jumlah_barang">Jumlah</label>
            <input required class="col" type="text" name="jumlah_barang">
          </div>
          <div class="row">
            <button type="submit">Simpan</button>
          </div>
      </form>

      <div>
        <button class="row my-4">
          <a href="<?= base_url('index.php/tampil') ?>">tampil</a>
        </button>
      </div>

      <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#inputBelanjaModal">
        Input Belanjaan
        </button>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#BelanjaModal">
        Belanja Lagi
        </button>

        <!-- Modal -->
        <div class="modal fade" id="inputBelanjaModal" tabindex="-1" aria-labelledby="inputBelanjaModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="inputBelanjaModalLabel">Belanjaan Baru</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= form_open('belanja/input'); ?>
                <div class="form-group">
                    <label for="namaBarang">Nama Barang</label>
                    <input type="text" name="namaBarang" id="namaBarang">
                </div>
                  <button type="submit" name="btn-simpan" id="btn-simpan" >Simpan</button>
                <?= form_close() ?>
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" name="simpan-barang-baru" id="simpan-barang-baru" class="btn btn-primary">Save changes</button>
            </div>
            </div>
        </div>
        </div>

    </div>


    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.min.js" integrity="sha384-VHvPCCyXqtD5DqJeNxl2dtTyhF78xXNXdkwX1CZeRusQfRKp+tA7hAShOK/B/fQ2" crossorigin="anonymous"></script>
    -->
<script type="text/javascript">
$(document).ready(function () {
  $("#simpan-barang-baru").click(function () {
       $("#btn-simpan").submit();
    });
});
</script>

    <?php 
      
    ?>

    <script src="<?= base_url("asset/js/belanja.js") ?>" type="text/javascript"></script>
  </body>
</html>